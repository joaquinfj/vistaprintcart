import unittest
from src.cart import Cart


class TestCart(unittest.TestCase):
    """
    It asserts the Cart class
    """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_cart_creation(self):
        """
        Test that it can sum a list of integers
        """
        items = [1, 2, 3]
        asserted_cart = Cart(items)
        self.assertIsInstance(asserted_cart, Cart)
        return True


if __name__ == '__main__':
    unittest.main()
