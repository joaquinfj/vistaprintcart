import unittest
from src.fixtures.cart_cases import *
from src.cart import Cart
from src.items import Items


class TestCalculatePrice(unittest.TestCase):
    """
    It asserts the Cart class
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_calculate_case_a(self):
        """
        Case A
        :return:
        """
        case_a = fixture_case_a(self)
        items = Items.parse_json_to_items(case_a)
        asserted_cart = Cart(items)
        self.assertEqual(asserted_cart.calculate_total_price(), 22.5)
        return True

    def test_calculate_case_b(self):
        """
        Case B
        :return:
        """
        case_b = fixture_case_b(self)
        items = Items.parse_json_to_items(case_b)
        asserted_cart = Cart(items)
        self.assertEqual(asserted_cart.calculate_total_price(), (40.5))
        return True

    def test_calculate_case_c(self):
        """
        Case B
        :return:
        """
        case_c = fixture_case_c(self)
        items = Items.parse_json_to_items(case_c)
        asserted_cart = Cart(items)
        self.assertEqual(asserted_cart.calculate_total_price(), 54)
        return True


if __name__ == '__main__':
    unittest.main()
