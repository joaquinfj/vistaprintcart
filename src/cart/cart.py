import datetime


class Cart:
    """
    """
    def __init__(self, items):

        self.total_original_price = 0
        self.total_price = 0
        self.items = items

    def calculate_original_price(self):
        self.total_original_price = self._sum_item_prices(self.items, original=True)
        return self.total_original_price

    def _sum_item_prices(self, items, original=False):
        total_price = 0
        for item in items:
            if not original:
                total_price += item.price
            else:
                total_price += item.original_price
        return total_price

    def calculate_total_price(self):
        for item in self.items:
            if item.coupon:
                self.items = item.coupon.apply_coupon_to_cart(self.items)
        self.total_price = self._sum_item_prices(self.items)
        return self.total_price

# end class Cart
