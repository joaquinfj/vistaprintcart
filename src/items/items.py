from src.items import Item, Coupon


class Items:
    """
    Helper class.
    @TODO: Build iterator for item??
    """

    @staticmethod
    def parse_json_to_items(json_datas):
        items = []
        position = 0
        for json_data in json_datas:
            position += 1
            items.append(Items.parse_json_to_item(json_data, position))
        return items

    @staticmethod
    def parse_json_to_item(json_data, position):
        coupon = None
        if json_data.get('coupon'):
            coupon = Coupon(json_data['coupon']['coupon_type'], json_data['coupon']['discount'],
                            json_data['coupon']['specific_type'])
        item = Item(json_data['price'], position, json_data['product_type'], coupon)
        return item

# End class Items___
