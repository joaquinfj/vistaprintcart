class Item:
    """
    Class that defines an item
    """

    def __init__(self, price, position, product_type, coupon=None):
        """
        Represent info on each of the item of a shopping cart
        :param price:
        :param position:
        :param product_type:
        :param coupon:
        """
        self.original_price = price
        self.price = price
        self.position = position
        self.product_type = product_type
        self.coupon = coupon

# End class Item
