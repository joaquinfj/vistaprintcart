import copy


class Coupon:
    """
    Class that defines a coupon. It could be 3 types of coupon at the moment.
    """

    def __init__(self, coupon_type, discount, specific_type = None):
        self.coupon_type = coupon_type
        self.discount = discount
        if specific_type and not specific_type == '':
            self.specific_type = specific_type
        else:
            self.specific_type = None

    def apply_coupon_to_cart(self, items):
        """
        :param items: Item
        :return:
        """
        if self.coupon_type == 'ALL':
            return self._apply_discount_to_all(items)
        elif self.coupon_type == 'SECOND_TYPE':
            return self._apply_discount_second_type(items)
        elif self.coupon_type == 'NEXT_ITEM':
            return self._apply_discount_next_product(items)

    # One function for each type of discount. We cold face this with deeper OOP.
    def _apply_discount_to_item(self, item):
        return item.price - item.price * (self.discount / 100)

    def _apply_discount_second_type(self, items):
        """
        self.coupon_type == 'SECOND_TYPE':
        :param items:
        :return:
        """
        new_items = []
        should_apply = 0
        for item in items:
            new_item = copy.deepcopy(item)

            if not item.coupon and item.product_type==self.specific_type:
                should_apply +=1
                if should_apply==2:
                    new_item.price = self._apply_discount_to_item(new_item)
                    should_apply = 0

            new_items.append(new_item)
        return new_items

    def _apply_discount_to_all(self, items):
        """
        self.coupon_type == 'ALL':
        :param items:
        :return:
        """
        new_items = []
        for item in items:
            new_item = copy.deepcopy(item)

            if not item.coupon:
                new_item.price = self._apply_discount_to_item(new_item)

            new_items.append(new_item)
        return new_items

    def _apply_discount_next_product(self, items):
        """
        self.coupon_type == 'NEXT_ITEM':
        :param items:
        :return:
        """
        new_items = []
        should_apply = False
        for item in items:
            new_item = copy.deepcopy(item)

            if item.coupon == self:
                should_apply = True
            elif should_apply:
                new_item.price = self._apply_discount_to_item(new_item)
                should_apply = False

            new_items.append(new_item)
        return new_items

    def __eq__(self, other):
        if not isinstance(other, Coupon):
            # don't attempt to compare against unrelated types
            return NotImplemented

        return self.coupon_type == other.coupon_type\
               and self.discount == other.discount and self.specific_type == other.specific_type

# End of Class Coupon
