import unittest
from src.fixtures.cart_cases import *
from src.items import Items


class TestLoader(unittest.TestCase):
    """
    It asserts the Cart class
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_items_loader(self):
        """
        Test item and coupon loader
        """
        case_a = fixture_case_a(self)
        items = Items.parse_json_to_items(case_a)
        self.assertEqual(items[0].price, case_a[0]['price'])
        return True


if __name__ == '__main__':
    unittest.main()
