import json


def fixture_case_a(self):
    """
    :return:
    """
    json_string = """
            [
              {
                "price": 25,
                "product_type": "A",
                "product_title": "Product of type A with price 25$"
              },
              {
                "price": 0,
                "product_type": "coupon",
                "product_title": "Coupon of 10% for all products.",
                "coupon": {
                    "coupon_type": "ALL",
                    "discount": 10,
                    "specific_type": ""
                }
              }
            ]      
            """
    data = json.loads(json_string)
    return data


def fixture_case_b(self):
    """
    :return:
    """
    json_string = """
            [
              {
                "price": 25,
                "product_type": "A",
                "product_title": "Product of type A with price 25$"
              },
              {
                "price": 0,
                "product_type": "coupon",
                "product_title": "Coupon of 10% for all products.",
                "coupon": {
                    "coupon_type": "ALL",
                    "discount": 10,
                    "specific_type": ""
                }
              },
              {
                "price": 0,
                "product_type": "coupon",
                "product_title": "Coupon of 20% for second product of type B.",
                "coupon": {
                    "coupon_type": "SECOND_TYPE",
                    "discount": 20,
                    "specific_type": "B"
                }
              },
              {
                "price": 10,
                "product_type": "B",
                "product_title": "Product of type B with price 10$"
              },
              {
                "price": 12.5,
                "product_type": "B",
                "product_title": "Product of type B with price 12.5$"
              }
            ]      
            """
    data = json.loads(json_string)
    return data

def fixture_case_c(self):
    """
    Case C - a shopping cart might contain:
  1. Product of type C with price 30$.
  2. Coupon of 25% for the next product in the shopping cart.
  3. Product of type B with price 10$.
  4. Coupon of 10% for all products.
  5. Product of type A with pruice 22.5$.
    :return:
    """
    json_string = """
            [
              {
                "price": 30,
                "product_type": "C",
                "product_title": "Product of type C with price 30$."
              },
              {
                "price": 0,
                "product_type": "coupon",
                "product_title": "Coupon of 25% for the next product in the shopping cart",
                "coupon": {
                    "coupon_type": "NEXT_ITEM",
                    "discount": 25,
                    "specific_type": ""
                }
              },
              {
                "price": 10,
                "product_type": "B",
                "product_title": "Product of type B with price 10$."
              },
              {
                "price": 0,
                "product_type": "coupon",
                "product_title": "Coupon of 10% for all products",
                "coupon": {
                    "coupon_type": "ALL",
                    "discount": 10,
                    "specific_type": ""
                }
              },
              {
                "price": 22.5,
                "product_type": "A",
                "product_title": "Product of type A with pruice 22.5."
              }
            ]      
            """
    data = json.loads(json_string)
    return data
