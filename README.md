# VistaPrint code challenge test

---

#### Candidate: Joaquín Forcada Jiménez
#### Date: 2019-May

---

### Description of the challenge, by VistaPrint

##### The assignment

Calculate the final price for a shopping cart having into account the following scenarios:


````
Case B - a shopping cart might contain:
 1. Product of type A with price 25$.
 2. Coupon of 10% for all products.
 3. Coupon of 20% for second product of type B.
 4. Product of type B with pprice 10$.
 5. Product of type B with price 12.5$.
````

```` 
Case C - a shopping cart might contain:
  1. Product of type C with price 30$.
  2. Coupon of 25% for the next product in the shopping cart.
  3. Product of type B with price 10$.
  4. Coupon of 10% for all products.
  5. Product of type A with pruice 22.5$.
````
 

***


## Installation
* Requirements **Python 3.6**, **pipenv**

* To install dependencies you can do:
    * Via **pipenv and PipFile**: please check **Pipfile** in the root directory if you want to find out about dependencies. You can install via:

        > pipenv install

        
## Test
You can change the fixtures within this folder:

> src/fixtures/cart_cases.py

* Run via unit tests, please execute.

> pipenv run python -m unittest discover -s src -p 'test_*.py


__

